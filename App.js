import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import LoginScreen from './src/screen/LoginScreen';
import MainNavigator from './src/navigator/MainNavigator';
import AppStack from './src/navigator/AppStack';
import Profile from './src/screen/Profile';
import SplashScreen from 'react-native-splash-screen';

const App = () => {
  SplashScreen.hide();
  return (
    <NavigationContainer>
      <AppStack/>
    </NavigationContainer>
  );
};

export default App;
