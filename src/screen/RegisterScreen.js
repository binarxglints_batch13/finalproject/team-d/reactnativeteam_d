import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import TxtInput from '../component/TxtInput';
import PassInput from '../component/PassInput';
import Button from '../component/Button';
import BtnGugel from '../component/BtnGugel';
import {useNavigation} from '@react-navigation/native';

const RegisterScreen = props => {
  const navigation = useNavigation();
  return (
    <View>
      <View style={style.img}>
        <Image
          source={require('../assets/CrowdFinder.png')}
          style={{height: 64, width: 230}}
        />
      </View>
      <TxtInput title="E-mail" />
      <PassInput title="Password" />
      <PassInput title="Re-enter Password" />
      <View style={style.button}>
        <Button
          title="Create Account"
          onPress={() => props.navigation.navigate('Main')}
        />
      </View>
      <BtnGugel title="Sign up with Google" />
      <View style={style.end}>
        <Text>Already have account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={{color: '#D82671', fontWeight: 'bold'}}>
            sign in here!
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default RegisterScreen;

const style = StyleSheet.create({
  img: {
    marginTop: 50,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 20,
    marginBottom: 20,
  },
  end: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
});
