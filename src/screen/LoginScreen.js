import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import TxtInput from '../component/TxtInput';
import PassInput from '../component/PassInput';
import Button from '../component/Button';
import BtnGugel from '../component/BtnGugel';

const LoginScreen = props => {
  return (
    <View>
      <View style={style.img}>
        <Image
          source={require('../assets/CrowdFinder.png')}
          style={{height: 64, width: 230}}
        />
      </View>
      <TxtInput title="E-mail" />
      <PassInput title="Password" />
      <View style={style.forget}>
        <TouchableOpacity>
          <Text>Forget Password?</Text>
        </TouchableOpacity>
      </View>
      <View style={style.button}>
        <Button
          title="Log In"
          onPress={() => props.navigation.navigate('Main')}
        />
      </View>
      <BtnGugel title="Log In with Google" />
      <View style={style.end}>
        <Text>Didn't have account? </Text>
        <TouchableOpacity onPress={() => props.navigation.navigate('Register')}>
          <Text style={{color: '#D82671', fontWeight: 'bold'}}>
            sign up here!
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginScreen;

const style = StyleSheet.create({
  img: {
    marginTop: 80,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  forget: {
    marginLeft: 40,
  },
  button: {
    marginTop: 35,
    marginBottom: 10,
  },
  end: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
});
