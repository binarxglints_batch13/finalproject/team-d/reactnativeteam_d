import { ceiling } from 'prelude-ls';
import React from 'react';
import {
  SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, useColorScheme, View, Image, Button, TouchableOpacity
} from 'react-native';
import colors from '../assets/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import TagCard from '../component/TagCard';
import CrowdName from '../component/CrowdName';
import AboutCard from '../component/AboutCard';
import ActivitiesCard from '../component/ActivitiesCard';


const Profile = () => {
  return (
    <ScrollView style={{ flex: 1, backgroundColor: colors.background, }}>
      <Image style={{ height: 150, width: 400, }} source={{ uri: 'https://images.unsplash.com/photo-1552160793-cbaf3ebcba72?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80' }}/>

      <View style={{ height: 110, width: 400, backgroundColor: colors.background, }}>
        <View style={{ left: 16, bottom: 60, width: 103, height: 103, backgroundColor: 'white', borderRadius: 100, position: 'absolute', zIndex: 1, }}>
          <View style={{ left: 6, top: 5, width: 90, height: 90, backgroundColor: 'red', borderRadius: 100, position: 'absolute', zIndex: 1, }} />
        </View>
        <Text style={styles.name}>Shaun Murphy</Text>
        <View style={styles.location}>
          <Ionicons name='location' size={15} color='black' />
          <Text style={{ fontFamily: 'Mulish', fontSize: 12, color: 'grey' }}>   Jakarta</Text>
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
          <TouchableOpacity style={styles.btn} >
            <Text style={{ color: colors.primary }}>About</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btn} >
            <Text style={{ color: colors.primary }}>Activities</Text>
          </TouchableOpacity>
        </View>
      </View>
      <AboutCard/>
      <ActivitiesCard/>
      <ActivitiesCard/>
    </ScrollView>

  );
}

export default Profile;

const styles = StyleSheet.create({
  btn: {
    height: 40,
    width: 160,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    backgroundColor: 'white',
    marginLeft: 20,
    marginRight: 20,
    borderBottomWidth: 3,
    borderBottomColor: colors.primary
  },
  name: {
    fontFamily: 'Mulish',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 20,
    marginLeft: 125,
    marginTop: 8,
    color: '#000000'
  },
  location: {
    marginTop: 5,
    lineHeight: 20,
    marginLeft: 125,
    flexDirection: 'row'

  },
  text: {
    fontFamily: 'Mulish',
    fontWeight: 'bold',
    fontSize: 16,
  }


})
