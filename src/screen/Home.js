import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import HeaderHome from '../component/HeaderHome';
import CreatePostInput from '../component/CreatePostInput';
import HomeEvent from '../component/HomeEvent';
import Feeds from '../component/Feeds';
import FeedsCom from '../component/FeedsCom';

const Home = () => {
  return (
    <View>
      <ScrollView>
        <HeaderHome />
        <CreatePostInput />
        <View style={style.Event}>
          <Text style={{fontWeight: 'bold'}}>Your Events</Text>
          <View style={{left: 200}}>
            <TouchableOpacity>
              <Text style={{color: '#D82671'}}>See All Events</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView horizontal={true}>
          <HomeEvent />
          <HomeEvent />
        </ScrollView>
        <View>
          <Text style={style.Feeds}>Feeds</Text>
        </View>
        <Feeds />
        <Feeds />
        <FeedsCom />
      </ScrollView>
    </View>
  );
};

export default Home;

const style = StyleSheet.create({
  Event: {
    flexDirection: 'row',
    marginTop: 30,
    marginLeft: 20,
  },
  Feeds: {
    marginLeft: 20,
    fontWeight: 'bold',
    marginTop: 20,
    fontSize: 16,
  },
});
