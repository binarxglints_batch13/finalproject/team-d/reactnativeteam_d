import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

const HeaderHome = () => {
  return (
    <View style={style.BGcolor}>
      <TextInput style={style.txtInput} />
    </View>
  );
};

export default HeaderHome;

const style = StyleSheet.create({
  BGcolor: {
    backgroundColor: '#D82671',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtInput: {
    borderWidth: 1,
    backgroundColor: 'white',
    width: 253,
    height: 36,
    margin: 5,
    borderRadius: 8,
  },
});
