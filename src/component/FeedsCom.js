import React, {useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import ProfilePic from './ProfilePic';
import AntDesign from 'react-native-vector-icons/AntDesign';

const FeedsCom = props => {
  return (
    <View style={{margin: 10, position: 'relative', alignItems: 'center'}}>
      <View style={{flexDirection: 'row', padding: 15}}>
        <View style={{padding: 5}}>
          <View
            style={{
              flexDirection: 'row',
              right: 80,
            }}>
            <ProfilePic />
            <View>
              <Text style={{marginLeft: 15, width: 130}} numberOfLines={1}>
                Community Name
              </Text>
              <Text style={{marginLeft: 15, marginTop: 5}}>3 hours ago</Text>
            </View>
            <View>
              <TouchableOpacity style={styles.DesBtn}>
                <Text style={{color: '#D82671'}}>Design</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.reviewCard}>
        <Text
          style={{
            margin: 10,
            textAlign: 'justify',
            fontSize: 14,
            letterSpacing: 0.4,
            lineHeight: 23,
          }}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem et duis
          mi ut donec varius mattis. Eget blandit amet aliquam tincidunt sed
          viverra morbi faucibus....
        </Text>
        <Image
          style={{height: 241, width: 311, marginTop: 10, borderRadius: 10}}
          source={{
            uri: 'https://images.unsplash.com/photo-1552160793-cbaf3ebcba72?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80',
          }}
        />
        <View style={styles.Bot}>
          <View>
            <TouchableOpacity
              style={{flexDirection: 'row', position: 'absolute', right: 80}}>
              <AntDesign name="like1" size={18} color="#D82671" />
              <Text style={{marginLeft: 5}}>Like(0)</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              style={{flexDirection: 'row', position: 'absolute', left: 60}}>
              <AntDesign name="message1" size={18} color="#D82671" />
              <Text style={{marginLeft: 5}}>Comments</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          marginTop: 30,
          marginLeft: 10,
          marginRight: 10,
          borderBottomWidth: 0.5,
          borderBottomColor: 'black',
        }}
      />
    </View>
  );
};

const reduxState = state => ({});

const reduxDispatch = dispatch => ({});

export default FeedsCom;

const styles = StyleSheet.create({
  reviewCard: {
    backgroundColor: 'white',
    marginLeft: 7,
    elevation: 10,
    borderRadius: 8,
    width: 360,
    height: 412,
    alignItems: 'center',
  },
  btn: {
    position: 'absolute',
    right: 10,
    top: 10,
  },

  profile: {
    height: 50,
    width: 50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 100 / 2,
  },
  add: {
    height: 50,
    width: 50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 100 / 2,
  },
  DesBtn: {
    height: 28,
    width: 88,
    borderColor: '#D82671',
    borderWidth: 1,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 70,
  },
  Bot: {
    flexDirection: 'row',
    marginTop: 15,
    position: 'relative',
  },
});
