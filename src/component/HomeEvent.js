import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
} from 'react-native';
import colors from '../assets/colors';
import {connect} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';

const HomeEvent = props => {
  return (
    <View style={styles.reviewCard}>
      <Image
        style={{height: 138, width: 262}}
        source={{
          uri: 'https://images.unsplash.com/photo-1552160793-cbaf3ebcba72?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80',
        }}
      />
      <Text style={{margin: 5, fontSize: 14, fontWeight: 'bold'}}>
        Festival Makanan Nusantara (Bintang tamu 3 juri masterchef)
      </Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          width: '100%',
          marginTop: 15,
        }}>
        <View style={{flexDirection: 'row'}}>
          <Ionicons name="calendar" size={15} color="black" />
          <Text style={{marginLeft: 10}}>22/07/2021</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Ionicons name="time" size={15} color="black" />
          <Text style={{marginLeft: 10}}>09:00 AM</Text>
        </View>
      </View>
      <View
        style={{
          marginTop: 30,
          marginLeft: 10,
          marginRight: 10,
          borderBottomWidth: 0.5,
          borderBottomColor: 'black',
        }}
      />
    </View>
  );
};

const reduxState = state => ({});

const reduxDispatch = dispatch => ({});

export default HomeEvent;

const styles = StyleSheet.create({
  reviewCard: {
    backgroundColor: 'white',
    margin: 15,
    elevation: 10,
    borderRadius: 8,
    width: 262,
    height: 237,
    alignItems: 'center',
  },
  btn: {
    position: 'absolute',
    right: 10,
    top: 10,
  },

  profile: {
    height: 50,
    width: 50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 100 / 2,
  },
  add: {
    height: 50,
    width: 50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 100 / 2,
  },
});
