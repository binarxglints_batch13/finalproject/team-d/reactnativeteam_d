import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity } from 'react-native'
import colors from '../assets/colors';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';



const ActivitiesCard = (props) => {
    return (
        <View style={{ margin: 10, }}>
            <View style={{ flexDirection: 'row', padding: 15, }}>
                <View>
                    <View style={styles.profile} />
                </View>
                <View style={{ padding: 5, }}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{ marginLeft: 10 }}>Shaun</Text>
                        <Text>   Post a story</Text>
                        <Text style={{ fontWeight: 'bold', color: colors.primary }}>AAA</Text>
                    </View>
                    <View>
                        <Text style={{ fontSize: 12, marginLeft: 10, marginTop: 5 }}>3 Hours ago</Text>
                    </View>
                </View>

            </View>
            <View style={styles.reviewCard}>
                <Image style={{ height: 120, width: 330, marginTop: 20, borderRadius: 10 }} source={{ uri: 'https://images.unsplash.com/photo-1552160793-cbaf3ebcba72?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80' }} />
                <Text style={{ margin: 15, fontSize: 16, fontWeight: 'bold' }}>Festival Makanan Nusantara (Bintang tamu 3 juri masterchef)</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: 300 }}>
                    <View style={{ flexDirection: 'row', }}>
                        <Ionicons name='calendar' size={15} color='black' />
                        <Text style={{ marginLeft: 20 }}>22/07/2021</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Ionicons name='time' size={15} color='black' />
                        <Text style={{ marginLeft: 20 }}>22/07/2021</Text>
                    </View>
                </View>
                <Text style={{ margin: 10, textAlign: 'justify', fontSize: 14, letterSpacing: 0.4 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem et duis mi ut donec varius mattis. Eget blandit amet aliquam tincidunt sed viverra morbi faucibus....</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Ionicons name='people' size={15} color='black' />
                    <Text>129 People attend this</Text>
                    <Text>See more</Text>

                </View>
            </View>
            <View style={{ marginTop: 30, marginLeft: 10, marginRight: 10, borderBottomWidth: 0.5, borderBottomColor: 'black' }} />
        </View>

    )
}

const reduxState = (state) => ({

})

const reduxDispatch = (dispatch) => ({

})

export default ActivitiesCard;

const styles = StyleSheet.create({
    reviewCard: {
        backgroundColor: 'white',
        marginLeft: 7,
        elevation: 10,
        borderRadius: 8,
        width: 360,
        height: 412,
        alignItems: 'center'
    },
    btn: {
        position: 'absolute',
        right: 10,
        top: 10
    },

    profile: {
        height: 50,
        width: 50,
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 100 / 2,
    },
    add: {
        height: 50,
        width: 50,
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 100 / 2,
    },

})
