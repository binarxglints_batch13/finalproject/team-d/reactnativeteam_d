import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import ProfilePic from './ProfilePic';
import AntDesign from 'react-native-vector-icons/AntDesign';

const CreatePostInput = () => {
  return (
    <View style={style.container}>
      <View style={style.txtInput}>
        <View style={style.pic}>
          <ProfilePic />
          <View style={{marginLeft: 5}}>
            <TextInput placeholder={'Create Post or Events'} />
          </View>
          <View style={style.plus}>
            <AntDesign name="pluscircle" size={32} color="#D82671" />
          </View>
        </View>
      </View>
    </View>
  );
};

export default CreatePostInput;

const style = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 10,
    alignItems: 'center',
    position: 'relative',
  },
  txtInput: {
    borderWidth: 1,
    width: 343,
    height: 67,
    borderRadius: 9,
    borderColor: '#E5E5E6',
  },
  pic: {
    position: 'absolute',
    top: 8,
    left: 12,
    flexDirection: 'row',
  },
  plus: {
    position: 'absolute',
    left: 280,
    top: 8,
  },
});
