import React from 'react';
import {View, ImageBackground, TouchableOpacity} from 'react-native';

const ProfilePic = props => {
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity onPress={props.onPress}>
        <ImageBackground
          source={{
            uri: 'https://images.unsplash.com/photo-1552160793-cbaf3ebcba72?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80',
          }}
          style={{
            height: 37,
            width: 37,
            borderRadius: 1000,
          }}
          imageStyle={{borderRadius: 1000}}
        />
      </TouchableOpacity>
    </View>
  );
};

export default ProfilePic;
