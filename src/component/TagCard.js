import React from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    useColorScheme,
    View,
    TouchableOpacity
} from 'react-native';

import colors from '../assets/colors';


const TagCard = (props) => {
    return (
        <View>
            <View style={styles.btn} >
                <Text style={{ color: colors.primary }}>{props.tags}</Text>
            </View>
        </View>
    );
}

export default TagCard;

const styles = StyleSheet.create({
    btn: {
        height: 40,
        width: 100,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: 'black',
    },

})