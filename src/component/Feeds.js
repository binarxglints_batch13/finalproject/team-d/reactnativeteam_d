import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
} from 'react-native';
import colors from '../assets/colors';
import {connect} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ProfilePic from './ProfilePic';

const Feeds = props => {
  return (
    <View style={{margin: 10, position: 'relative', alignItems: 'center'}}>
      <View style={{flexDirection: 'row', padding: 15}}>
        <View style={{padding: 5}}>
          <View
            style={{
              flexDirection: 'row',
              right: 80,
            }}>
            <ProfilePic />
            <View>
              <Text style={{marginLeft: 15, width: 130}} numberOfLines={1}>
                Shaun post a story
              </Text>
              <Text style={{marginLeft: 15, marginTop: 5}}>3 hours ago</Text>
            </View>
            <View>
              <TouchableOpacity style={styles.DesBtn}>
                <Text style={{color: '#D82671'}}>Design</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.reviewCard}>
        <Image
          style={{height: 120, width: 330, marginTop: 20, borderRadius: 10}}
          source={{
            uri: 'https://images.unsplash.com/photo-1552160793-cbaf3ebcba72?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80',
          }}
        />
        <Text style={{margin: 15, fontSize: 16, fontWeight: 'bold'}}>
          Festival Makanan Nusantara (Bintang tamu 3 juri masterchef)
        </Text>
        <View
          style={{
            flexDirection: 'row',

            width: '100%',
          }}>
          <View style={{flexDirection: 'row', paddingLeft: 15}}>
            <Ionicons name="calendar" size={15} color="black" />
            <Text style={{marginLeft: 10}}>22/07/2021</Text>
          </View>
          <View style={{flexDirection: 'row', left: 260, position: 'absolute'}}>
            <Ionicons name="time" size={15} color="black" />
            <Text style={{marginLeft: 10}}>09:00 AM</Text>
          </View>
        </View>
        <Text
          style={{
            margin: 10,
            textAlign: 'justify',
            fontSize: 14,
            letterSpacing: 0.4,
            lineHeight: 20,
          }}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem et duis
          mi ut donec varius mattis. Eget blandit amet aliquam tincidunt sed
          viverra morbi faucibus....
        </Text>
        <View
          style={{flexDirection: 'row', position: 'relative', marginTop: 20}}>
          <View style={{flexDirection: 'row', right: 1, position: 'absolute'}}>
            <Ionicons name="people" size={15} color="black" />
            <Text style={{marginLeft: 10}}>129 People attend this</Text>
          </View>
          <View>
            <TouchableOpacity style={{position: 'absolute', left: 90}}>
              <Text style={{color: '#D82671'}}>See Details</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          marginTop: 30,
          marginLeft: 10,
          marginRight: 10,
          borderBottomWidth: 0.5,
          borderBottomColor: 'black',
        }}
      />
    </View>
  );
};

const reduxState = state => ({});

const reduxDispatch = dispatch => ({});

export default Feeds;

const styles = StyleSheet.create({
  reviewCard: {
    backgroundColor: 'white',
    marginLeft: 7,
    elevation: 10,
    borderRadius: 8,
    width: 360,
    height: 412,
    alignItems: 'center',
  },
  btn: {
    position: 'absolute',
    right: 10,
    top: 10,
  },

  profile: {
    height: 50,
    width: 50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 100 / 2,
  },
  add: {
    height: 50,
    width: 50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 100 / 2,
  },
  DesBtn: {
    height: 28,
    width: 88,
    borderColor: '#D82671',
    borderWidth: 1,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 70,
  },
});
