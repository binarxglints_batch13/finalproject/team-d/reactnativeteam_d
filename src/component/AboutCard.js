import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import colors from '../assets/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import TagCard from './TagCard';
import CrowdName from './CrowdName';

const AboutCard = props => {
    return (
        <View>
            <View style={{ borderBottomWidth: 0.5, margin: 20 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                    <Text style={styles.text}>Bio</Text>
                    <Ionicons name='create' size={20} color='black' />
                </View>
                <Text style={{ textAlign: 'justify', fontFamily: 'Mulish', marginBottom: 20 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem consectetur suspendisse et, libero id magna. Gravida eu auctor aliquet posuere accumsan sit et. Orci sodales turpis augue sit eu metus.</Text>
            </View>
            <View style={{}}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20, marginBottom: 10 }}>
                    <Text style={styles.text}>Interest Topic</Text>
                    <Ionicons name='create' size={20} color='black' />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <TagCard tags='Food' />
                    <TagCard tags='Food' />
                    <TagCard tags='Food' />
                </View>
            </View>
            <View style={{ margin: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                    <Text style={styles.text}>My Crowd</Text>
                    <Text style={{ color: colors.primary, borderBottomWidth: 0.5, borderBottomColor: colors.primary }}>See All</Text>
                </View>
                <View>
                    <CrowdName />
                </View>
            </View>
        </View>
    );
};

export default AboutCard;


const styles = StyleSheet.create({
  btn: {
    height: 40,
    width: 160,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    backgroundColor: 'white',
    marginLeft: 20,
    marginRight: 20,
    borderBottomWidth: 3,
    borderBottomColor: colors.primary
  },
  name: {
    fontFamily: 'Mulish',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 20,
    marginLeft: 125,
    marginTop: 8,
    color: '#000000'
  },
  location: {
    marginTop: 5,
    lineHeight: 20,
    marginLeft: 125,
    flexDirection: 'row'

  },
  text: {
    fontFamily: 'Mulish',
    fontWeight: 'bold',
    fontSize: 16,
  }


})