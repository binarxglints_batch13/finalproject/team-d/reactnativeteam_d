import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import colors from '../assets/colors';



const CrowdName = (props) => {
    return (

        <View style={styles.reviewCard}>
            <View>
                <View style={{ flexDirection: 'row', }}>
                    <View>
                        <View style={styles.profile} />
                    </View>
                    <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                        <Text>Community Name</Text>
                        <Text style={{ marginTop: 5, fontSize: 12 }}>Member</Text>
                    </View>
                    <View>
                        <TouchableOpacity style={styles.btn} >
                            <Text style={{ color: 'white',}}>Unfollow</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        </View>
    )
}

const reduxState = (state) => ({

})

const reduxDispatch = (dispatch) => ({

})

export default CrowdName;

const styles = StyleSheet.create({
    reviewCard: {
        backgroundColor: 'white',
        padding: 15,
        elevation: 10,
        borderRadius: 8
    },

    profile: {
        height: 50,
        width: 50,
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 100 / 2,
    },
    btn: {
        height: 40,
        width: 100,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.primary,
        marginLeft: 50,
        borderRadius: 10,
        marginTop: 5
      },

})
