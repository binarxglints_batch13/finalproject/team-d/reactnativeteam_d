import React from 'react';
import {View, TextInput, Text, StyleSheet} from 'react-native';

const PassInput = props => {
  return (
    <View>
      <View style={{marginLeft: 25}}>
        <Text style={style.txt}>{props.title}</Text>
      </View>
      <View style={style.container}>
        <TextInput
          style={style.PassInput}
          underlineColorAndroid="transparent"
          secureTextEntry={true}
          onChangeText={props.input}
          value={props.value}
        />
      </View>
    </View>
  );
};

export default PassInput;

const style = StyleSheet.create({
  container: {justifyContent: 'center', alignItems: 'center', margin: 10},
  PassInput: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'grey',
    width: '90%',
    color: 'black',
    paddingLeft: 10,
  },
  txt: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: 10,
    fontWeight: 'bold',
  },
});
