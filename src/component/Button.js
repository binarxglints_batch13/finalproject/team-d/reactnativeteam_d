import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

const Button = props => {
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity
        style={{
          borderWidth: 1,
          borderRadius: 12,
          width: '88%',
          alignItems: 'center',
          justifyContent: 'center',
          height: 50,
          backgroundColor: '#D82671',
        }}
        onPress={() => props.onPress()}>
        <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>
          {props.title}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Button;
