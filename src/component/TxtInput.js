import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

const TxtInput = props => {
  return (
    <View>
      <View style={{marginLeft: 25}}>
        <Text style={styles.txt}>{props.title}</Text>
      </View>
      <View style={styles.container}>
        <TextInput
          style={styles.TxtInput}
          onChangeText={props.input}
          value={props.value}
          numberOfLines={props.numberOfLines}
          placeholder={props.placeholder}
        />
      </View>
    </View>
  );
};

export default TxtInput;

const styles = StyleSheet.create({
  TxtInput: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'grey',
    width: '90%',
    color: 'black',
    paddingLeft: 10,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  txt: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    color: 'black',
    padding: 10,
    fontWeight: 'bold',
  },
});
