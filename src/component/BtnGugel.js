import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const BtnGugel = props => {
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity
        style={{
          borderWidth: 1,
          borderRadius: 12,
          width: '88%',
          alignItems: 'center',
          justifyContent: 'center',
          height: 50,
        }}
        onPress={() => props.onPress()}>
        <View style={{flexDirection: 'row'}}>
          <View style={{marginRight: 5}}>
            <AntDesign name="google" size={25} color="black" />
          </View>
          <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold'}}>
            {props.title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default BtnGugel;
