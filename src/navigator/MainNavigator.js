import React, { useEffect } from 'react'
import { Image, View, TouchableOpacity, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Notification from "../screen/Notification"
import Home from '../screen/Home'



import colors from '../assets/colors';
import Profile from "../screen/Profile";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();



// const homeStack = (props) => {
//     return (
//         <Stack.Navigator>
//             <Stack.Screen options={{ headerShown: false }} name="Home" component={Homescreen} />
//             <Stack.Screen options={{ headerTintColor: colors.primary }} name='Detail' component={detailScreen} />
//             <Stack.Screen options={{ headerTintColor: colors.primary }} name='Review' component={reviewMovieScreen} />
//         </Stack.Navigator>
//     )
// }

// const profileStack = (props) => {
//     return (
//         <Stack.Navigator>
//             <Stack.Screen options={{ headerShown: false }} name="Profile" component={Profile} />
//             <Stack.Screen options={{ headerTintColor: colors.primary }} name='EditProfile' component={editProfile} />
//         </Stack.Navigator>
//     )
// }

const MainNavigator = (props) => {

    return (
        <Tab.Navigator 
            tabBarOptions={{
                showLabel: false,
            }}

        >
            <Tab.Screen name="Home" component={Home}
                options={{ headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <View style={{ alignItems: 'center', justifyContent: 'center', }}>
                            <Ionicons name={'home'} size={30} color={focused ? colors.primary : 'grey'} />
                        </View>
                    ),
                }}
            />
            <Tab.Screen name="Notification" component={Notification}
                options={{ headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <View style={{ alignItems: 'center', justifyContent: 'center', }}>
                            <Ionicons name={'notifications'} size={30} color={focused ? colors.primary : 'grey'} />
                        </View>
                    ),
                }}
            />
            <Tab.Screen name="Profile" component={Profile}
                options={{ headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <View style={{ alignItems: 'center', justifyContent: 'center', }}>
                            <Ionicons name={'person'} size={30} color={focused ? colors.primary : 'grey'} />
                        </View>
                    ),
                }} />
        </Tab.Navigator>
    )
}

const style = StyleSheet.create({
    shadow: {
        shadowColor: '#7F5DF0',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.5,
        elevation: 5
    }
})



const reduxState = state => ({


})

const reduxDispatch = dispatch => ({


})

export default MainNavigator;
